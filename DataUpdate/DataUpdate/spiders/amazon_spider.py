import scrapy, re, csv
from time import sleep
from ..items import AmazonscrapItem
from .urls_extractor import calculate_list
from pymongo import MongoClient
import socket

# video_mongo_ip = '34.225.140.74'
# mongo_video = MongoClient('mongodb://' + '127.0.0.1' + ':27017/')
# db = mongo_video['videocommerce_demo_6']
# videoproduct = db['videoproducts']
import pandas as pd

file_name = 'DataUpdate/spiders/zee_oos.csv'  # copy the file in same folder to read the stuff
d, httpstatus_list_404,httpstatus_list_202_no_data = {}, [],[]
file_name_update = 'DataUpdate/spiders/zee_oos_update.csv'  # copy the file in same folder to read the stuff

count = 0
selling_prize_new, selling_prize_string = [], ''


class AmazonSpiderSpider(scrapy.Spider):
	name = 'amazon'
	allowed_domains = ['amazon.com']

	custom_settings = {

		'DOWNLOADER_MIDDLEWARES': {
			'DataUpdate.middlewares.RotatingProxyMiddleware': 610,
			'DataUpdate.middlewares.BanDetectionMiddleware': 620,
		},
		'PROXY_FETCHING_API': 'http://list.didsoft.com/get?email=ruman@accio.ai&pass=wqw4qi&pid=httppremium&showcountry=no',
		'PROXY_REFRESH_INTERVAL': 10000,  # seconds
		'DOWNLOAD_TIMEOUT': 50

	}

	def start_requests(self):
		urls = calculate_list(1)
		for i in urls:
			yield scrapy.Request(url=i, callback=self.parse)

	def parse(self, response, **kwargs):
		item = AmazonscrapItem()

		a, b, c = response.css("#priceblock_ourprice::text").getall(), response.css(
			"#priceblock_saleprice::text").getall(), response.css("#priceblock_dealprice::text").getall()
		sellingstring = select_one(a, b, c)

		# sellingstring= response.xpath("//*[(@id = 'priceblock_ourprice')]")
		# sellingstring = response.css("#priceblock_ourprice::text,#priceblock_saleprice::text,#priceblock_dealprice::text").getall()

		print("\n\n\nSELLiNG PRIZE", sellingstring, type(sellingstring))

		if response.status == 404:
			httpstatus_list_404.append(response.request.url)

		if len(sellingstring) == 0 and response.status != 404:
			# isempty = True
			selling_prize = '0'
			item['selling_prize'] = selling_prize
		else:
			# isempty = False
			selling_prize_string = sellingstring[0].replace(',', '')
			selling_prize = re.findall('\d+,?', str(selling_prize_string))

		if len(selling_prize) > 0:
			sell_prize = selling_prize[0].lstrip('0')
			sell_prize = sell_prize.replace(',', '')
			selling_prize_new.append(sell_prize)
			item['selling_prize'] = sell_prize

		d[response.request.url] = item['selling_prize']  # , isempty

		print("dictonary length", len(d))
		print("URL:", response.request.url)
		print("PRINT= ", item['selling_prize'], type(item['selling_prize']), len(item['selling_prize']))
		print("======================================================\n\n")

		return

	def close(spider, reason):
		print("Updating CSV now..........\n\n")
		print("dictonary length", len(d))
		list_junk = []
		with open(file_name, 'r') as csvfile:
			csvreader = csv.reader(csvfile)
			with open(file_name_update, "w") as file:
				writer = csv.writer(file)
				for rows in csvreader:
					if rows[4] in d.keys():
						s_prize = d[rows[4]]
						s_prize = s_prize.split('.')[0]
						print("d[rows[4]]=", d[rows[4]], ",S_prize=", s_prize, "S_prize_type=", type(s_prize),
							  "S_prize_len=", len(s_prize), "rows[5]=", rows[5],
							  type(rows[5]))
						if s_prize == '' or s_prize == None or len(s_prize) == 0:
							rows[6] = 'FALSE'
						elif (s_prize) != (rows[5]):
							rows[8] = str(s_prize)
							#updating selling prize in zee_oos_update.csv
						elif (s_prize) == (rows[5]):
							print("This data is where we need no change...")
						else:
							pass

					writer.writerow(rows)
		with open("404_status_links.csv", "w") as file:
			writer = csv.writer(file)
			for link in httpstatus_list_404:
				writer.writerow(link)
		# print("Total number of data been read......\n", len(d), d)


def select_one(a, b, c):
	if len(a):
		return a
	elif len(b):
		return b
	elif len(c):
		return c
	else:
		return []
